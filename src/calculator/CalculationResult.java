/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package calculator;

import java.util.Date;

/**
 *
 * @author dwm77
 */
public class CalculationResult {

    private float remaining_balance;
    private Date time;
    private int current_month;
    private float surplus;
    private float interest;
    private float surplusLessInterest;
    private float expences;
    private float income;
    private float mortgage_remaining_balance;

    public void setMonth(int current_month) {
        this.current_month = current_month;
    }

    public void setDate(Date time) {
        this.time = time;
    }

    public void setBalance(float remaining_balance) {
        this.remaining_balance = remaining_balance;
    }

    public void setSurplus(float this_month_surplus) {
        this.surplus = this_month_surplus;
    }

    public void setInterest(float this_month_interest) {
        this.interest = this_month_interest;
    }

    public void setSurplusLessInterest(float this_month_surplus_less_interest) {
        this.surplusLessInterest = this_month_surplus_less_interest;
    }

    public void setExpences(float monthlyExpences) {
        this.expences = monthlyExpences;
    }

    public void setIncome(float monthlyIncome) {
        this.income = monthlyIncome;
    }

    public void setMortgageRemainingBalance(float remaining_balance) {
        this.mortgage_remaining_balance = remaining_balance;
    }

    public float getBalance() {
        return remaining_balance;
    }

    public float getMortgageRemainingBalance() {
        return mortgage_remaining_balance;
    }

    public Date getDate() {
        return time;
    }

    public float getExpences() {
        return expences;
    }

    public float getIncome() {
        return income;
    }

    public float getInterest() {
        return interest;
    }

    public int getMonth() {
        return current_month;
    }

    public float getSurplus() {
        return surplus;
    }

    public float getSurplusLessInterest() {
        return surplusLessInterest;
    }
}
