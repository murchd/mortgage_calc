/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package calculator;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

/**
 *
 * @author dwm77
 */
public class MortgageManager {

    private float base_rate;
    private int year_term;
    private float monthly_interest_rate;
    private float annual_interest_rate;
    private float annual_interest_percent;
    private float loan_amount;
    private int month_term;
    private float monthly_factor;
    private float monthly_payment;
    private float principal;
    private int current_month;
    private int current_year;
    private float total_interest_paid;
    private float this_year_interest_paid;
    private float this_year_principal_paid;
    private float principal_paid;
    private float remaining_balance;
    private float interest_paid;
    private float total_spent_this_year;
    private Calculation calculation;
    private Calendar currentDateDay;
    private float monthly_payment_stored;

    public MortgageManager(Calculation calculation) {
        this.calculation = calculation;
        currentDateDay = Calendar.getInstance();
        currentDateDay.setTime(calculation.getStartDate());

        loan_amount = calculation.getLoanAmount();
        annual_interest_percent = Float.parseFloat(calculation.getInterestRate());
        year_term = calculation.getTerm();

        month_term = year_term * 12;
        annual_interest_rate = annual_interest_percent / 100;
        monthly_interest_rate = annual_interest_rate / 12;
        monthly_factor = calculateInterestFactor();
        monthly_payment = loan_amount / monthly_factor;
        principal = loan_amount;
        current_month = 1;
        current_year = 1;
        monthly_payment_stored = calculateMonthlyPayment();

    }

    private Float calculateInterestFactor() {
        float factor = 0;
        base_rate = 1 + monthly_interest_rate;
        float denominator = base_rate;
        for (int i = 0; i < (year_term * 12); i++) {
            factor += (1 / denominator);
            denominator *= base_rate;
        }
        return factor;
    }

    private Float calculateMonthlyPayment() {
        int power = -(month_term);
        float denom = (float) Math.pow((1 + monthly_interest_rate), power);
        monthly_payment = principal * (monthly_interest_rate / (1 - denom));
        return monthly_payment;
    }

    public List<CalculationResult> calculate() {
        List results = new ArrayList();
        while (current_month <= month_term) {
            interest_paid = principal * monthly_interest_rate;
            principal_paid = monthly_payment - interest_paid;
            remaining_balance = principal - principal_paid;

            this_year_interest_paid = this_year_interest_paid + interest_paid;
            this_year_principal_paid = this_year_principal_paid + principal_paid;

            CalculationResult result = new CalculationResult();
            result.setMonth(current_month);
            result.setDate(currentDateDay.getTime());
            result.setMortgageRemainingBalance(remaining_balance);
            results.add(result);


            total_interest_paid += interest_paid;
            principal = remaining_balance;
            current_month++;
            currentDateDay.add(Calendar.MONTH, 1);

        }
        return results;
    }

    public int getYears(int months) {
        int years = months / 12;
        return (int) Math.floor(years);
    }

    public int getMonths(int months) {
        months = months % 12;
        return months;
    }

    public int getMonth_term() {
        return month_term;
    }

    public float getMonthly_payment_stored() {
        return this.monthly_payment_stored;
    }

    public float getTotal_interest_paid() {
        return this.total_interest_paid;
    }
}
