/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package calculator;

import java.util.Date;

/**
 *
 * @author dwm77
 */
public class Calculation {

    private Integer calculationId;
    private String title;
    private Date startDate;
    private Integer term;
    private Long loanAmount;
    private Integer budgetSettingsId;
    private String username;
    private String interestRate;
    private Integer lastRun;

    public Calculation() {
    }

    public Calculation(Integer calculationId) {
        this.calculationId = calculationId;
    }

    public Calculation(Integer calculationId, String interestRate) {
        this.calculationId = calculationId;
        this.interestRate = interestRate;
    }

    public Integer getCalculationId() {
        return calculationId;
    }

    public void setCalculationId(Integer calculationId) {
        this.calculationId = calculationId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Integer getTerm() {
        return term;
    }

    public void setTerm(Integer term) {
        this.term = term;
    }

    public Long getLoanAmount() {
        return loanAmount;
    }

    public void setLoanAmount(Long loanAmount) {
        this.loanAmount = loanAmount;
    }

    public Integer getBudgetSettingsId() {
        return budgetSettingsId;
    }

    public void setBudgetSettingsId(Integer budgetSettingsId) {
        this.budgetSettingsId = budgetSettingsId;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getInterestRate() {
        return interestRate;
    }

    public void setInterestRate(String interestRate) {
        this.interestRate = interestRate;
    }

    public Integer getLastRun() {
        return lastRun;
    }

    public void setLastRun(Integer lastRun) {
        this.lastRun = lastRun;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (calculationId != null ? calculationId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Calculation)) {
            return false;
        }
        Calculation other = (Calculation) object;
        if ((this.calculationId == null && other.calculationId != null) || (this.calculationId != null && !this.calculationId.equals(other.calculationId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "nz.net.lifechangers.entities.Calculation[ calculationId=" + calculationId + " ]";
    }
}
