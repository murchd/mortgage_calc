/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package calculator;

import java.util.Date;

/**
 *
 * @author davidm
 */
public class mortgage_calc {

    public static void main(String[] args) {
        new mortgage_calc().run();
    }

    public void run() {
        Calculation calc = new Calculation();
        calc.setInterestRate("7");
        calc.setLoanAmount(200000L);
        calc.setTerm(20);
        calc.setStartDate(new Date());
        MortgageManager manager = new MortgageManager(calc);
        manager.calculate();
        System.out.println("Monthy Payment: " + manager.getMonthly_payment_stored());
        System.out.println("Total Interest Paied: " + manager.getTotal_interest_paid());
    }
}
